package nl.jdriven;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class WishListSessionThreeTest {
    /* Start hier
    private WishList list;
    private WishListItem item;


    // Create a new WishList class
    @Test
    public void wishListExists() {
        assertNotNull(WishList.class);
    }


    // Create a new WishListItem class
    @Test
    public void wishListItemExists() {
        assertNotNull(WishListItem.class);
    }

    // Add a method for adding WishListItems to the WishList class
    @Test
    public void addItemToList() {
        list = new WishList();
        item = new WishListItem();
        list.addItem(item);
        assertEquals(1, list.getItemCount());
    }


    // Add name and price to the wish list item
    @Test
    public void addNameAndPriceToItem() {
        list = new WishList();
        item = new WishListItem();
        list.addItem(item);

        item.setName("Toy robot");
        item.setPrice(9.99);

        list.addItem(new WishListItem("Mindstorm robot", 299.95));

        assertEquals("Toy robot", list.getWishListItems()[0].getName());
        assertEquals(9.99, list.getWishListItems()[0].getPrice());
        assertEquals("Mindstorm robot", list.getWishListItems()[1].getName());
        assertEquals(299.95, list.getWishListItems()[1].getPrice());
    }


    // Add multiple items in one go
    @Test
    public void addMultipleItemsInOneGo() {
        list = new WishList();
        list.addItem(
                new WishListItem("Sailboat", 250000.00),
                new WishListItem("Toy robot", 9.99),
                new WishListItem("Mindstorm robot", 299.95),
                new WishListItem("Speedboat", 430000.00)
        );

        assertEquals(4, list.getItemCount());
    }


    // Get total value
    @Test
    public void getTotalValue() {
        list = new WishList();
        list.addItem(
                new WishListItem("Sailboat", 250000.00),
                new WishListItem("Toy robot", 9.99),
                new WishListItem("Mindstorm robot", 299.95),
                new WishListItem("Speedboat", 430000.00)
        );

        assertEquals(680309.94, list.getTotalValue());
    }


    // Get average price
    @Test
    public void getAveragePrice() {
        list = new WishList();
        list.addItem(
                new WishListItem("Sailboat", 250000.00),
                new WishListItem("Toy robot", 9.99),
                new WishListItem("Mindstorm robot", 299.95),
                new WishListItem("Speedboat", 430000.00)
        );

        assertEquals(170077.485, list.getAveragePrice());
    }


    // Sort items by name ( Think about the compareTo which was used in the book )
    @Test
    public void sortByName() {
        list = new WishList();
        list.addItem(
                new WishListItem("Sailboat", 250000.00),
                new WishListItem("Toy robot", 9.99),
                new WishListItem("Mindstorm robot", 299.95),
                new WishListItem("Speedboat", 430000.00)
        );

        list.sortByName();

        assertEquals("Mindstorm robot", list.getWishListItems()[0].getName());
        assertEquals("Sailboat", list.getWishListItems()[1].getName());
        assertEquals("Speedboat", list.getWishListItems()[2].getName());
        assertEquals("Toy robot", list.getWishListItems()[3].getName());
    }


    // Sort items by name
    @Test
    public void sortByPrice() {
        list = new WishList();
        list.addItem(
                new WishListItem("Sailboat", 250000.00),
                new WishListItem("Toy robot", 9.99),
                new WishListItem("Mindstorm robot", 299.95),
                new WishListItem("Speedboat", 430000.00)
        );

        list.sortByPrice();

        assertEquals(9.99, list.getWishListItems()[0].getPrice());
        assertEquals(299.95, list.getWishListItems()[1].getPrice());
        assertEquals(250000.0, list.getWishListItems()[2].getPrice());
        assertEquals(430000.0, list.getWishListItems()[3].getPrice());
    }
    */
}
